package eu.singlemarket.thirdcountry.access;

public interface MemberAccessedExternally {
	public void acceptForeignGoods(ForeignGoods fg);
}
