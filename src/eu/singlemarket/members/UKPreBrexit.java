package eu.singlemarket.members;

import eu.singlemarket.Capital;
import eu.singlemarket.Goods;
import eu.singlemarket.Member;
import eu.singlemarket.Person;
import eu.singlemarket.Services;

public class UKPreBrexit implements Member {

	public void sellFinancialServicesToMemberOfEU(Member m) {
		m.acceptServices(new Services("Latest Ponzi Scheme"));
		System.out.println("Ker - ching £! €! $!");
	}
	
	public static void main(String...args) {
		new UKPreBrexit().sellFinancialServicesToMemberOfEU(new France());
	}
	

	@Override
	public void acceptCapital(Capital c) {
		System.out.println("There you go, keys to your flat in Chelsea.");
	}

	@Override
	public void acceptGoods(Goods g) {
		System.out.println("BMWs?  Prosecco?  Lovely.");
	}

	@Override
	public void acceptServices(Services s) {
		System.out.println("Thank goodness, someone who can run a railway!");
	}

	@Override
	public void acceptLabour(Person p) {
		System.out.println("Oh dear.  Some of us are a bit narrow minded");
	}
}
