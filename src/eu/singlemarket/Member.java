package eu.singlemarket;

public interface Member {
	public void acceptLabour(Person p);
	public void acceptCapital(Capital c);
	public void acceptGoods(Goods g);
	public void acceptServices(Services s);
}
